




<?php

if (isset($_POST) && (is_array($_POST)) && (array_key_exists('pid', $_POST))) {
	$pid = $_POST['pid'];
	$curl_handle=curl_init();
	if ($curl_handle === null) {
		echo "Manca curl";
		die;
	}
	curl_setopt($curl_handle, CURLOPT_URL, 'https://fc.cab.unipd.it/fedora/get/o:'.$pid.'/bdef:Content/get');
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 20);
	curl_setopt($curl_handle, CURLOPT_TIMEOUT, 20);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// scarica file immagine
	$bufferImmagine = curl_exec($curl_handle);
	curl_close($curl_handle);
	
	// copia immagine in locale
	if ($bufferImmagine !== false) {
		echo "Lettura immagine riuscita<br />";
		//echo "<pre>" . $bufferImmagine . "</pre>";
		$destinazioneImmagine = "immagini/";
		$nomeFileImg = $destinazioneImmagine . $pid . ".jpeg";
		$imgHandle = fopen($nomeFileImg, "wb");
		if ($imgHandle !== false) {
			if (is_writable($nomeFileImg)) {
				$byteScritti = fwrite($imgHandle, $bufferImmagine);
				if ($byteScritti === false) {
					echo "Caricamento immagine fallito<br />";
				} else {
					echo "Immagine caricata: " . $nomeFileImg . " (" . $byteScritti . " byte scritti)<br />";
					// aggiungo record immagine nel DB

				}
				fclose($imgHandle);
			} else {
				echo "File non scrivibile: " . $nomeFileImg . "<br />";
			}
		} else {
			echo "impossibile aprire il file " . $nomeFileImg . "<br />";
		}
	} else {
		echo "Lettura immagine fallita<br />";
	}
}

$dir = opendir('immagini');
$images = array();
$html = '';
while($fname = readdir($dir)) {
     
     if(preg_match('/[.]jpeg$/', $fname)) {
         
         $images[] = $fname;
         
     }
     
 }  
 
closedir($dir);

function imgSort($a, $b)
 {
     $a1 = str_replace('.jpegg', '', $a);
     $a2 = str_replace('.jpeg', '', $b);
     
     return $a1 > $a2;
     
 }
 
usort($images, 'imgSort');





foreach($images as $img) {
     
     $html .= '<img src="immagini/' . $img . '" alt="" />' . "\n";
     
 }
 
 echo $html;

     




